﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SliderSetter : MonoBehaviour
{
    public Slider slider;
    public FloatVariable floatVariable;

    void Update()
    {
        if(slider != null && floatVariable != null)
        {
            slider.value = floatVariable.floatValue;
        }
    }
}
