﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Events/GameEvent")]
public class GameEvent : ScriptableObject
{
    List<GameEventListener> evenListeners = new List<GameEventListener>();
    
    public void Raise()
    {
        int count = (evenListeners.Count-1);
        for (int i=count; i>=0; i--)
        {
            evenListeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if(!evenListeners.Contains(listener))
        {
            evenListeners.Add(listener);
        }
    }

    public void UnregisterListener(GameEventListener listener)
    {
        if (evenListeners.Contains(listener))
        {
            evenListeners.Remove(listener);
        }
    }
}
