﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloatVariableEventListener : MonoBehaviour
{
    [Tooltip("Event to register with")]
    public FloatVariable gameEvent;

    [Tooltip("Response to invoke when Event is raised")]
    public UnityEvent response;

    private void OnEnable()
    {
        gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        gameEvent.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        response.Invoke();
    }
}
