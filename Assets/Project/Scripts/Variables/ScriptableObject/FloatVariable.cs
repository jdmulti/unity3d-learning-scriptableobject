﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Variables/FloatVariable")]
public class FloatVariable : ScriptableObject
{
    List<FloatVariableEventListener> evenListeners = new List<FloatVariableEventListener>();

    public float floatValue;
    public Vector2 range;
    public bool isEvent;

    void Raise()
    {
        if(isEvent)
        {
            int count = (evenListeners.Count - 1);
            for (int i = count; i >= 0; i--)
            {
                evenListeners[i].OnEventRaised();
            }
        }
    }

    public void RegisterListener(FloatVariableEventListener listener)
    {
        if (!evenListeners.Contains(listener))
        {
            evenListeners.Add(listener);
        }
    }

    public void UnregisterListener(FloatVariableEventListener listener)
    {
        if (evenListeners.Contains(listener))
        {
            evenListeners.Remove(listener);
        }
    }

    public void SetValue(float value)
    {
        if(range.x == 0f && range.y == 0f)
        {
            floatValue = value;
        }
        else if(value >= range.x && value <= range.y)
        {
            floatValue = value;
        }
        else if(value < range.x)
        {
            floatValue = range.x;
        }
        else if(value > range.y)
        {
            floatValue = range.y;
        }
        Raise();
    }
    public void Add(float value)
    {
        SetValue((floatValue + value));
    }
}
