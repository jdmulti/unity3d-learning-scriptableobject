﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonClass<T> : MonoBehaviour where T: MonoBehaviour
{
    static T instance = null;
    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GameObject.FindObjectOfType<T>();
            }
            return instance;
        }
    }
}
