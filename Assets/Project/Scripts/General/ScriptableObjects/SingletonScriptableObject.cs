﻿using System.Linq;
using UnityEngine;

/*
    source: http://baraujo.net/unity3d-making-singletons-from-scriptableobjects-automatically/
*/

public abstract class SingletonScriptableObject<T> : ScriptableObject where T: ScriptableObject
{
    static T instance = null;
    public static T Instance
    {
        get
        {
            if(!instance)
            {
                // SO is an assets, this needs a search within project resources
                instance = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
            }
            return instance;
        }
    }
}
