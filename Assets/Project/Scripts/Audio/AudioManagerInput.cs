﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerInput : MonoBehaviour
{
    public FloatVariable Volume;
    public GameEvent OnButtonClick;
    public float addAmount;

    void Update()
    {
        Controls();
    }

    void Controls()
    {
        if(Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Volume.Add(addAmount);
            OnButtonClick.Raise();
        }
        else if(Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Volume.Add(-addAmount);         
            OnButtonClick.Raise();
        }
    }
}
