﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Audio/AudioManagerSettings")]
public class AudioManagerSettings : ScriptableObject
{
    public int maxAudioSources;
    public AudioFile[] audioFiles;
}
