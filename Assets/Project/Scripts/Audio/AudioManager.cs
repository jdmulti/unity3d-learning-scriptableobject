﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    /*################################################################################
        Variables
    ################################################################################*/
    /*--------------------------------------------------------------------------------
        Reference
    --------------------------------------------------------------------------------*/
    public FloatVariable volume;
    public AudioManagerSettings audioManagerSettings;

    GameObject parent;
    AudioSource[] audioSources;
    AudioClip[] audioClips;

    /*################################################################################
        Functions: unity
    ################################################################################*/
    /*--------------------------------------------------------------------------------
        Awake
    --------------------------------------------------------------------------------*/
    public void Awake()
    {
        parent = this.gameObject;
        audioSources = AddAudioSources(audioManagerSettings.maxAudioSources, parent);
    }

    /*################################################################################
        Functions: custom public
    ################################################################################*/
    /*--------------------------------------------------------------------------------
        Play
    --------------------------------------------------------------------------------*/
    public void Play(AudioType audioType, bool loop=false)
    {
        audioClips = GetAudioClips(audioType);
        PlayAudioClip(audioSources, audioClips, volume.floatValue, loop);
    }
    // overload
    public void Play(AudioType audioType)
    {
        audioClips = GetAudioClips(audioType);
        PlayAudioClip(audioSources, audioClips, volume.floatValue, false);
    }
    /*--------------------------------------------------------------------------------
        PlayMusic
    --------------------------------------------------------------------------------*/
    public void PlayMusic(AudioType audioType)
    {
        audioClips = GetAudioClips(audioType);
        if(audioSources.Length > 0)
        {
            AudioSource source = audioSources[0];
            if(source.clip.name != audioClips[0].name)
            {
                source.clip = audioClips[0];
                source.volume = volume.floatValue;
                source.loop = true;
                source.Play();
            }
        }
    }
    /*--------------------------------------------------------------------------------
        Volume
    --------------------------------------------------------------------------------*/
    public void Volume(FloatVariable floatVariable)
    {
        SetAudioSourceVolume(audioSources, floatVariable.floatValue);
    }

    /*################################################################################
        Functions: custom private
    ################################################################################*/
    /*--------------------------------------------------------------------------------
        AddAudioSources
    --------------------------------------------------------------------------------*/
    AudioSource[] AddAudioSources(int count, GameObject parent)
    {
        for(int i=0; i< count; i++)
        {
            parent.AddComponent<AudioSource>();
        }
        return parent.GetComponents<AudioSource>();
    }
    /*--------------------------------------------------------------------------------
        GetAudioClips
    --------------------------------------------------------------------------------*/
    AudioClip[] GetAudioClips(AudioType audioType)
    {
        List<AudioClip> audioClips = new List<AudioClip>();
        AudioFile[] audioFiles = audioManagerSettings.audioFiles;
        int count = audioFiles.Length;
        for (int i = 0; i < count; i++)
        {
            if (audioFiles[i].audioType == audioType)
            {
                audioClips.Add(audioFiles[i].audioClip);
            }
        }
        if(audioClips.Count > 0)
        {
            return audioClips.ToArray();
        }
        else
        {
            return null;
        }
    }
    /*--------------------------------------------------------------------------------
        PlayAudioClip
    --------------------------------------------------------------------------------*/
    void PlayAudioClip(AudioSource[] sources, AudioClip clip, float volume, bool loop)
    {
        AudioSource source;
        bool isSoundPlayed = false;
        int count = sources.Length;
        for(int i=0; i< count; i++)
        {
            source = sources[i];
            if(source.isPlaying == false)
            {
                isSoundPlayed = true;
                source.clip = clip;
                source.volume = volume;
                source.loop = loop;
                source.Play();
                break;
            }
        }
        // check: if sound played at all, if not, there weren't enough audio sources
        if(isSoundPlayed == false)
        {
            source = parent.AddComponent<AudioSource>();
            source.clip = clip;
            source.volume = volume;
            source.loop = loop;
            source.Play();
            StartCoroutine(DestroyDelayed(source, clip.length));
        }
    }
    // overload
    void PlayAudioClip(AudioSource[] sources, AudioClip[] clips, float volume, bool loop)
    {
        if(clips != null && clips.Length > 0)
        {
            PlayAudioClip(sources, clips[Random.Range(0, clips.Length)], volume, loop);
        }
    }
    /*--------------------------------------------------------------------------------
        DestroyDelayed
    --------------------------------------------------------------------------------*/
    IEnumerator DestroyDelayed(AudioSource audioSource, float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(audioSource);
    }
    /*--------------------------------------------------------------------------------
        SetAudioSourceVolume
    --------------------------------------------------------------------------------*/
    void SetAudioSourceVolume(AudioSource[] source, float value)
    {
        int count = source.Length;
        for(int i=0; i< count; i++)
        {
            source[i].volume = value;
        }
    }
}
