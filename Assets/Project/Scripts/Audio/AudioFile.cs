﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioFile
{
    public AudioType audioType;
    public AudioClip audioClip;
}
