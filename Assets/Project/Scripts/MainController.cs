﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
    public AudioManager audioManager;
    public AudioType audioMusic;

    void Start()
    {
        audioManager.Play(audioMusic, true);
    }
}
