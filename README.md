unity3d-learning-scriptableobject

Info

This project is created while I explorer the useage of ScriptableObjects within Unity3D

Sources

Unite Austin 2017 - Game Architecture with Scriptable Objects
https://www.youtube.com/watch?time_continue=3406&v=raQ3iHhE_Kk

Unity3D: Making singletons from ScriptableObjects automatically
http://baraujo.net/unity3d-making-singletons-from-scriptableobjects-automatically/

Changelog Generator
https://www.npmjs.com/package/gitlab-changelog-generator/tutorial
https://docs.gitlab.com/ce/development/changelog.html#how-to-generate-a-changelog-entry
